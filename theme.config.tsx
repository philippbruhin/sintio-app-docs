import React from 'react'
import { DocsThemeConfig } from 'nextra-theme-docs'
import Logo from './components/Logo'

const config: DocsThemeConfig = {
  footer: {
    text: 'Nextra Docs Template',
  },

  i18n: [
    { locale: 'en', text: 'English' },
    { locale: 'de', text: 'Deutsch' }
  ],

  toc: {
    title: 'Index'
  },

  logo: (
    <Logo />
  ),
}

export default config
